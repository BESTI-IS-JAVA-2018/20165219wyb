import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;
public class ComplexTest extends TestCase {
  Complex a=new Complex(1,2);
  Complex b=new Complex(-2,-1);
  Complex d=new Complex(4,-2);
  @Test
  public void testequals(){
      assertEquals(false,a.equals(b));
      assertEquals(false,b.equals(c));
      assertEquals(true,new Complex(1.0,2.0).equals(a));
  }

  @Test
  public void testAdd(){
      assertEquals(new Complex(-1,1),a.ComplexAdd(b));
      assertEquals(new Complex(5,0),a.ComplexAdd(c));
  }
  @Test
  public void testSubtract(){
      assertEquals(new Complex(3,3),a.ComplexSubtract(b));
      assertEquals(new Complex(-3,4),a.ComplexSubtract(c));
  }
  @Test
  public void testMultiply(){
      assertEquals(new Complex(0,-5),a.ComplexMultiply(b));
      assertEquals(new Complex(17,4),new Complex(3,2.5).ComplexMultiply(c));
  }
  @Test
  public void testDivide(){
      assertEquals(new Complex(0,0.5),a.ComplexDivide(c));
      assertEquals(new Complex(-2,-1),b.ComplexDivide(new Complex(1,0)));
  }
}
