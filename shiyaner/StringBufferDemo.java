public class StringBufferDemo{
    StringBuffer buffer = new StringBuffer();
    public StringBufferDemo(StringBuffer buffer){
        this.buffer = buffer;
    }
    public Character charAt(int i){
        return buffer.charAt(i);
    }
    public int capacity(){
        return buffer.capacity();
    }
    public int length(){
        return buffer.length();
    }
    public int indexOf(String buf) {
        return buffer.indexOf(buf);
    }

}
